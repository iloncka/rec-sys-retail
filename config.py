import os


ROOT_PATH = os.getcwd()
DATA_PATH = os.path.join(ROOT_PATH, 'data')
RAW_DATA_PATH = os.path.join(DATA_PATH, 'raw')
CLEANED_DATA_PATH = os.path.join(DATA_PATH, 'interim')
PREDICT_DATA_PATH = os.path.join(DATA_PATH,'processed')
TRAINED_MODELS_PATH = os.path.join(ROOT_PATH,'models')

from pathlib import Path
path = Path("data\raw\market_sales.csv")
print(path.parent.absolute())

# CLEANED_USER_DATA_PATH = os.path.join(CLEANED_DATA_PATH, 'cleaned_user_data.csv')
# # PREPARED_DATA_PATH = "data/processed/prepared_data.csv"
# # PREPARED_DATA_PATH_TEST = "data/processed/test_data.csv"
# MODEL_PATH = "models/svd_model.pkl"
# CROSS_VAL_RESULTS_PATH = "reports/cross_val_results.txt"


