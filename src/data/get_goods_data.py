from typing import List

import numpy as np
import pandas as pd
import streamlit as st

# import click

#NB cost changed to sum_sale
# @click.command()
# @click.argument('input_path', type=click.Path(exists=True))
# @click.argument('output_path', type=click.Path())


def get_goods_data(cleaned_data: pd.DataFrame):
  """summary of the items in the cleaned dataframe

  Args:
      cleaned_data (pd.DataFrame): cleaned pd.DataFrame 

  Returns:
      [pd.DataFrame]: goods pd.DataFrame
  """

  # cleaned_data = pd.read_csv(input_path)
  cleaned_data['quantity'] = np.where(cleaned_data.sum_sale > 0, 1, -1)
  items_full = cleaned_data.groupby('item_id').agg({'store_id': 'count',
                                                    'license': 'max',
                                                    'ABC_analise_item': 'min',
                                                    #   'rating': 'max',
                                                    'sum_sale': 'sum',
                                                    'quantity': 'sum'}).reset_index()
  items_full.rename(columns={'store_id': 'in_stores',
                             'sum_sale': 'amount'}, inplace=True)
  items_full.sort_values('quantity', ascending=False, inplace=True)
  # items_full.to_csv(output_path, index = False)
  return items_full


if __name__ == '__main__':
    get_goods_data(df)
