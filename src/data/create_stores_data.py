# -*- coding: utf-8 -*-
import os
import glob
from pathlib2 import Path
import pandas as pd
import numpy as np
import streamlit as st
from typing import List
from collections import defaultdict
# import click
from clearml import Task, Dataset


DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')

MIN_NUM_PURCHASES = 10
CLEANED_DATA_PATH = os.path.join(DATA_PATH, 'interim')

def filter_by_purchases(df: pd.DataFrame, min_num_purchases=MIN_NUM_PURCHASES):

    users_by_purchases = df['user_id'].value_counts().reset_index()
    users_by_purchases = users_by_purchases.rename(columns={'index': 'user_id', 'user_id': 'purchases'})
    users_id_list = users_by_purchases[users_by_purchases['purchases'] > min_num_purchases]['user_id']
    users_filter = df['user_id'].isin(users_id_list)
    cleaned_user_data = df[users_filter]

    return cleaned_user_data
# @click.command()
# @click.argument('input_filepath', type=click.Path(exists=True))
# @click.argument('output_filepath', type=click.Path())
# @click.argument('store_position', type=click.INT)
# @click.argument('min_num_purchases', type=click.INT)
def create_stores_data(input_path=CLEANED_DATA_PATH, 
                 output_path=CLEANED_DATA_PATH, 
                 ):
    task = Task.init(project_name="mlops-rec-sys",
                    task_name="Step3 create_stores_data",
                    task_type=Task.TaskTypes.data_processing,
                    tags=['store','filter', 'dataset','v1'])

    # task.execute_remotely(queue_name="default")
    conf = {
        'dataset_id': '',
        'project_name': 'mlops-rec-sys',
        'source_dataset_name': 'clean-data',
        'input_path': CLEANED_DATA_PATH,
        'output_path': CLEANED_DATA_PATH,
        'filters':['min_num_purchases'],
        'min_num_purchases': MIN_NUM_PURCHASES
        }
    print('Configuration: {}'.format(conf))
    conf = task.connect(conf)

    if conf['dataset_id']:
        dataset = Dataset.get(dataset_id=conf.get('dataset_id', ''))
    else:
        dataset = Dataset.get(dataset_project=conf.get('project_name', "mlops-rec-sys"),
                            dataset_name=conf.get('source_dataset_name', 'raw-data'))
    input_path = dataset.get_local_copy()

    stores_dataset = Dataset.create(
    dataset_name='store-data',
    dataset_project="mlops-rec-sys",
    dataset_tags=['store dataset v1'],
    parent_datasets=None,
    use_current_task=False,
    )
    conf['output_dataset_id'] = stores_dataset.id
    logger = stores_dataset.get_logger()

    filtered_stores = defaultdict(list)
    for i, filename in enumerate(glob.glob(os.path.join(input_path, '*.csv')), 1):

        df = pd.read_csv(filename)
        new_f = Path(filename).name

        for store_id in pd.unique(df.store_id.values):

            store_data = df[df['store_id'] == store_id]
            filtered_store_df = filter_by_purchases(store_data, MIN_NUM_PURCHASES)
            filtered_stores[store_id].append(filtered_store_df)

    final_df_ls = []
    stores_dict = defaultdict(int)
    for store_id, stores in filtered_stores.items():
        
        store_df = pd.concat(stores)
        stores_dict[store_id] = store_df.shape[0]
        output_file_path = os.path.join(output_path, f'cleaned_flt_{MIN_NUM_PURCHASES}_purch_store_id_{store_id}.csv')
        store_df.to_csv(output_file_path, index=False)
        stores_dataset.add_files(output_file_path)
        descr_df_num = pd.DataFrame(store_df.describe(datetime_is_numeric=True,
                                                    include=None).T)
        logger.report_table(
                    title=f"Filtered-store-{store_id}-numerical-description",
                    series="pandas DataFrame",
                    iteration=0,
                    table_plot=descr_df_num,
                        )
        descr_df_cat = pd.DataFrame(store_df.describe(datetime_is_numeric=True,
                                                    include=object).T)
        logger.report_table(
                    title=f"Filtered-store-{store_id}-categorical-description",
                    series="pandas DataFrame",
                    iteration=0,
                    table_plot=descr_df_cat,
                        )
        logger.report_table(
                    title=f"Filtered-store-{store_id}-head",
                    series="pandas DataFrame",
                    iteration=0,
                    table_plot=store_df.head(),
                        )
        # stores_dataset.upload(show_progress=True)
        final_df_ls.append(store_df)

    stores_dataset.upload(show_progress=True)
    conf['stores_dict'] = stores_dict
    final_df = pd.concat(final_df_ls)
    final_df.to_csv(os.path.join(output_path, f'filtered_min_purch_{MIN_NUM_PURCHASES}_stores_all.csv'), index=False)
    stores_stats = pd.DataFrame(final_df['store_id'].value_counts().reset_index())
    stores_stats.to_csv(os.path.join(output_path, f'filtered_min_purch_{MIN_NUM_PURCHASES}_stores_stats.csv'), index=False)
    descr_df = pd.DataFrame(final_df.describe(datetime_is_numeric=True,
                                                    include=None).T)
    logger.report_table(
                title="Fltered_stores-numerical-description-all",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df,
                        )
    descr_df = pd.DataFrame(final_df.describe(datetime_is_numeric=True,
                                                    include=object).T)
    logger.report_table(
                title="Fltered_stores-categorical-description-all",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df,
                        )
    logger.report_table(
                title="Stats on filtered stores by num purchases",
                series="pandas DataFrame",
                iteration=0,
                table_plot=stores_stats,
                        )
    # clean_dataset.upload(show_progress=True)
    stores_dataset.finalize()

if __name__ == "__main__":

    create_stores_data()








