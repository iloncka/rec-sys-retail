import os
from clearml import Task
from clearml.automation import PipelineController

from dotenv import load_dotenv

load_dotenv()

MINIO_BUCKET = os.environ.get("MINIO_BUCKET")

MIN_NUM_PURCHASES = 10

DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')
RAW_DATA_PATH = os.path.join(*[DATA_PATH, 'raw', 'market_sales.csv'])
CLEANED_DATA_PATH = os.path.join(DATA_PATH, 'interim')

def pre_execute_callback_example(a_pipeline, a_node, current_param_override):
    # type (PipelineController, PipelineController.Node, dict) -> bool
    print(
        "Cloning Task id={} with parameters: {}".format(
            a_node.base_task_id, current_param_override
        )
    )
    # if we want to skip this node (and subtree of this node) we return False
    # return True to continue DAG execution
    return True


def post_execute_callback_example(a_pipeline, a_node):
    # type (PipelineController, PipelineController.Node) -> None
    print("Completed Task id={}".format(a_node.executed))
    # if we need the actual executed Task: Task.get_task(task_id=a_node.executed)
    return


# Connecting ClearML with the current pipeline,
# from here on everything is logged automatically
pipe = PipelineController(
    name="Data Pipeline", 
    project="mlops-rec-sys", 
    version="0.0.3", 
    auto_version_bump=True, 
    add_pipeline_tags=False
)

pipe.add_parameter(
    "bucket",
     MINIO_BUCKET,
    "Minio bucket",
)
pipe.add_parameter(
    "local_data_path",
     RAW_DATA_PATH,
    "Local data path",
)

pipe.add_parameter(
    "min_num_purchases",
     MIN_NUM_PURCHASES,
    "Min num purchases",
)
pipe.add_parameter(
    "cleaned_data_path",
     CLEANED_DATA_PATH,
    "Cleaned data path",
)
pipe.add_parameter(
    "filtered_stores_data_path",
     CLEANED_DATA_PATH,
    "Filtered stores data path",
)


pipe.add_step(
    name="Step1_load_raw_data",
    base_task_project="mlops-rec-sys",
    base_task_name="Step1 load_raw_data",
    parameter_override={"General/bucket": "${pipeline.bucket}",
                        "General/local_data_path": "${pipeline.local_data_path}"},
)

pipe.add_step(
    name="Step2_clean_data",
    parents=["Step1_load_raw_data"],
    base_task_project="mlops-rec-sys",
    base_task_name="Step2 clean_data",
    parameter_override={
                        "General/input_path": "${pipeline.local_data_path}",
                        "General/output_path": "${pipeline.cleaned_data_path}"},
    pre_execute_callback=pre_execute_callback_example,
    post_execute_callback=post_execute_callback_example,
)
pipe.add_step(
    name="Step3_create_stores_data",
    parents=["Step2_clean_data"],
    base_task_project="mlops-rec-sys",
    base_task_name="Step3 create_stores_data",
    parameter_override={
                        "General/input_path": "${pipeline.cleaned_data_path}",
                        "General/output_path": "${pipeline.filtered_stores_data_path}",
                        "General/min_num_purchases": "${pipeline.min_num_purchases}"
                        },
)
pipe.set_default_execution_queue("default")
# for debugging purposes use local jobs
pipe.start_locally()


# # Starting the pipeline (in the background)
# pipe.start()
# # Wait until pipeline terminates
# pipe.wait()
# # cleanup everything
# pipe.stop()

print("done")
