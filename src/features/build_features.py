"""

add because pylint

Module by build features. This module using functions from clean_data.py script
"""

import numpy as np
import pandas as pd
import os
import glob
from pathlib2 import Path
# import pickle
from clearml import Task, Dataset
import feature_engineering as fe


DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')


CLEANED_DATA_PATH = os.path.join(DATA_PATH, 'interim')
# from src.data.clean_data import load_row_data, clean_data
FEATURES_DATA_PATH = os.path.join(DATA_PATH, 'processed')



def build_features(source_dataset_name='clean-data', target_column='item_id'):
    """

    This function call pipeline for rename columns of row data, if needed,
    generate 6 types of user ratings, and add them to incoming dataframe.

    :param row_data_url: str, URL of row data
    :param target_columns: str, target item for building recommend system.
    "item_id" if we build recommendations for top goods per user,
    "subgroup_id" if we build recommendations for top groups of item per user

    :return: DF with obfuscation information about client, store, goods etc.,
    and 6 types of ratings for user-item recommended system
    """
    # print('Downloading data from cloud...')

    # # loading row data to pd.DataFrame
    # row_data = load_row_data(row_data_url)

    # print("Row data:", row_data)

    # print('\n')
    # print(80 * '*')
    # print('Cleaning data...')

    # # rename columns if needed
    # if 'Период' in row_data.columns:
    #     incoming_data = rename_columns(row_data)
    # else:
    #     incoming_data = row_data.copy()
    task = Task.init(project_name="mlops-rec-sys",
                    task_name="Step4 build_features",
                    task_type=Task.TaskTypes.data_processing,
                    tags=['features', 'dataset','v2'])

    # for check if code work properly
    # Task.set_offline(True)
    # task.execute_remotely(queue_name="default")

    # program arguments
    # Use either dataset_task_id to point to a tasks artifact or
    # use a direct url with dataset_url
    conf = {
        'dataset_id':'',
        'project_name': 'mlops-rec-sys',
        'source_dataset_name': source_dataset_name,
        'input_path': CLEANED_DATA_PATH,
        'output_path': FEATURES_DATA_PATH,
        'target_column': target_column,
        'sum_coef':1,
        'pop_coef':0.5
        }
    conf = task.connect(conf)
    print('Configuration: {}'.format(conf))

    target_column = conf.get('target_column', target_column)
    input_path = conf.get('input_path', CLEANED_DATA_PATH)
    output_path = conf.get('output_path', FEATURES_DATA_PATH)
    sum_coef = conf.get('sum_coef', 1)
    pop_coef = conf.get('pop_coef', 0.5)


    if conf['dataset_id']:
        dataset = Dataset.get(dataset_id=conf.get('dataset_id', ''))
    else:
        dataset = Dataset.get(dataset_project=conf.get('project_name', "mlops-rec-sys"),
                            dataset_name=conf.get('source_dataset_name', source_dataset_name))

    input_path = dataset.get_local_copy()

    preprocessed_dataset = Dataset.create(
    dataset_name='preprocessed-data',
    dataset_project='mlops-rec-sys',
    dataset_tags=['preprocessed dataset v2'],
    parent_datasets=None,
    use_current_task=False,
    )
    conf['out_preprocessed_dataset_id'] = preprocessed_dataset.id
    logger = preprocessed_dataset.get_logger()


    # "process" data
    # input_path = os.path.join(RAW_DATA_PATH, 'market_sales.csv')
    preprocessed_data = []

    for i, filename in enumerate(glob.glob(os.path.join(input_path, '*.csv')), 1):

        incoming_data = pd.read_csv(filename)
        new_f = Path(filename).name
        sum_list_nom, popularity_list_nom = fe.make_sum_and_pop_ratings(incoming_data,
                                                                        target_column=target_column)
        ratings = fe.make_ratings(sum_list_nom,
                                  popularity_list_nom,
                                  sum_coef=sum_coef,
                                  pop_coef=pop_coef,
                                  target_column=target_column)
        rec_sys_data_df = fe.prep_data(incoming_data,
                                    ratings,
                                    target_column=target_column)

        output_file_path = os.path.join(output_path, f'preprocessed_{new_f}')
        rec_sys_data_df.to_csv(output_file_path, index=False)
        preprocessed_dataset.add_files(output_file_path)
        descr_df_num = pd.DataFrame(rec_sys_data_df.describe(datetime_is_numeric=True,
                                                    include=None).T)
        logger.report_table(
                    title=f"Preprocessed-numerical-description-{i}",
                    series="pandas DataFrame",
                    iteration=i,
                    table_plot=descr_df_num,
                        )
        descr_df_cat = pd.DataFrame(rec_sys_data_df.describe(datetime_is_numeric=True,
                                                    include=object).T)
        logger.report_table(
                    title=f"Preprocessed-categorical-description-{i}",
                    series="pandas DataFrame",
                    iteration=i,
                    table_plot=descr_df_cat,
                        )
        logger.report_table(
                    title=f"Preprocessed-head-{i}",
                    series="pandas DataFrame",
                    iteration=i,
                    table_plot=rec_sys_data_df.head(),
                        )
        preprocessed_data.append(rec_sys_data_df)

    preprocessed_dataset.upload(show_progress=True)


    final_df = pd.concat(preprocessed_data)
    final_df.to_csv(os.path.join(output_path, 'preprocessed_all.csv'), index=False)

    descr_df_num = pd.DataFrame(final_df.describe(datetime_is_numeric=True,
                                                include=None).T)
    logger.report_table(
                title=f"Preprocessed-numerical-description-all",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df_num,
                    )
    descr_df_cat = pd.DataFrame(final_df.describe(datetime_is_numeric=True,
                                                include=object).T)
    logger.report_table(
                title=f"Preprocessed-categorical-description-all",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df_cat,
                    )
    # clean_dataset.upload(show_progress=True)
    preprocessed_dataset.finalize()

if __name__ == "__main__":

    build_features()

