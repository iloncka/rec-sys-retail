import os
import pandas as pd
# import click
import joblib as jb
from surprise import SVD
from surprise import accuracy
# from surprise.model_selection import GridSearchCV
# from surprise.model_selection import cross_validate
from surprise.model_selection import train_test_split
# from typing import List
import streamlit as st

from clearml import Task
import prepare_dataset as pdt


DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
MODELS_PATH = os.path.join(ROOT_PATH, 'models')

# @click.command()
# @click.argument("input_paths", type=click.Path(exists=True), nargs=2)
# @click.argument("output_paths", type=click.Path(), nargs=2)
# @click.argument("grid_search", type=click.BOOL)
# @click.argument("alg", type=click.types)
# @click.argument("opt_by", type=click.STRING)
def train_model(source_dataset_name='preprocessed-data',
                    rating_column = 'rating_div_S_on_P',
                    output_dir = MODELS_PATH):
    
    model_task = Task.init(project_name="mlops-rec-sys",
                    task_name="Step-6 train_model",
                    # task_type=Task.TaskTypes.training,
                    tags=['train', 'model','v2'],
                    output_uri=True)
    
    # тренировочные данные
    data = pdt.prepare_dataset(source_dataset_name=source_dataset_name,
                                rating_column = rating_column)
    
    # test_data = train_data.build_anti_testset()
    # Опциональнй гридсёрч по заданным параметрам
    
    trainset, testset = train_test_split(data, test_size=.25)

    algo = SVD() # KNNBasic()
    
    # Train the algorithm on the trainset, and predict ratings for the testset
    algo.fit(trainset)
    predictions = algo.test(testset)

    # Then compute RMSE
    rmse = accuracy.rmse(predictions)
    output_path = os.path.join(output_dir, f'model_{rating_column}_{rmse}.pkl')
    jb.dump(algo, output_path)
    loaded_model = jb.load(output_path)
    model_task.close()
    # clearml-serving - CLI for launching ClearML serving engine
    # New Serving Service created: id=351aef490dc347a18d4f669cf8254943

if __name__ == '__main__':
    train_model()


