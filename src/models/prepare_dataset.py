# import click
import pandas as pd
import surprise as sp

import streamlit as st
from typing import List
import os
import glob
from pathlib2 import Path
# import pickle
from clearml import Task, Dataset



DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')

# from src.data.clean_data import load_row_data, clean_data
FEATURES_DATA_PATH = os.path.join(DATA_PATH, 'processed')
PREPROCESSED_DATA_PATH = os.path.join(DATA_PATH, 'processed')
# @click.command()
# @click.argument("input_filepath", type=click.Path(exists=True))
# @click.argument("output_file_paths", type=click.Path(), nargs=2)
def prepare_dataset(source_dataset_name='preprocessed-data',
                    rating_column = 'rating_sum_S_and_P'):

    # rating_cols_dict = {'v1': ['popularity',
    #               'sum_sale_log',
    #               'popularity_log',
    #               'rating_sum_S_and_P',
    #               'rating_div_S_on_P']}
    
    task = Task.init(project_name="mlops-rec-sys",
                    task_name="Step5 prepare_dataset",
                    task_type=Task.TaskTypes.data_processing,
                    tags=['prepared', 'dataset','v2'])

    # for check if code work properly
    # Task.set_offline(True)
    # task.execute_remotely(queue_name="default")

    # program arguments
    # Use either dataset_task_id to point to a tasks artifact or
    # use a direct url with dataset_url
    conf = {
        'dataset_id':'',
        'project_name': 'mlops-rec-sys',
        'source_dataset_name': source_dataset_name,
        'input_path': FEATURES_DATA_PATH,
        'output_path': PREPROCESSED_DATA_PATH,
        'rating_column': rating_column
        
        }
    conf = task.connect(conf)
    print('Configuration: {}'.format(conf))

    rating_column = conf.get('rating_column', rating_column)
    input_path = conf.get('input_path', FEATURES_DATA_PATH)
    output_path = conf.get('output_path', PREPROCESSED_DATA_PATH)
    

    if conf['dataset_id']:
        dataset = Dataset.get(dataset_id=conf.get('dataset_id', ''))
    else:
        dataset = Dataset.get(dataset_project=conf.get('project_name', "mlops-rec-sys"),
                            dataset_name=conf.get('source_dataset_name', source_dataset_name))

    input_path = dataset.get_local_copy()

    preprocessed_dataset = Dataset.create(
    dataset_name=f'ready-data-{rating_column}',
    dataset_project='mlops-rec-sys',
    dataset_tags=['ready dataset v2'],
    parent_datasets=None,
    use_current_task=False,
    )
    conf['out_ready_dataset_id'] = preprocessed_dataset.id
    logger = preprocessed_dataset.get_logger()


    # "process" data
    # input_path = os.path.join(RAW_DATA_PATH, 'market_sales.csv')
    preprocessed_data = []

    for i, filename in enumerate(glob.glob(os.path.join(input_path, '*.csv')), 1):

        df = pd.read_csv(filename, dtype={'user_id': str,                                
                                          'item_id': str})
        print(df.shape)
        prepared_df = df[['user_id', 'item_id', rating_column]].reset_index(drop=True)
        prepared_df.columns = ['user_id', 'item_id', 'rating']
        print(prepared_df.shape)
        print(type(prepared_df))
        new_f = Path(filename).name
        output_file_path = os.path.join(output_path, f'ready_{new_f}')
        prepared_df.to_csv(output_file_path, index=False)
        preprocessed_dataset.add_files(output_file_path)
        descr_df_num = pd.DataFrame(prepared_df.describe(include=None).T)
        logger.report_table(
                    title=f"Ready-numerical-description-{i}",
                    series="pandas DataFrame",
                    iteration=i,
                    table_plot=descr_df_num,
                        )
        descr_df_cat = pd.DataFrame(prepared_df.describe(include=object).T)
        logger.report_table(
                    title=f"Ready-categorical-description-{i}",
                    series="pandas DataFrame",
                    iteration=i,
                    table_plot=descr_df_cat,
                        )
        logger.report_table(
                    title=f"Ready-head-{i}",
                    series="pandas DataFrame",
                    iteration=i,
                    table_plot=prepared_df.head(),
                        )
        preprocessed_data.append(prepared_df)

    preprocessed_dataset.upload(show_progress=True)
    
    print(len(preprocessed_data))

    if len(preprocessed_data) > 1:
        final_df = pd.concat(preprocessed_data)
    else:
        final_df = prepared_df
    final_df.to_csv(os.path.join(output_path, 'ready_all.csv'), index=False)

    descr_df_num = pd.DataFrame(final_df.describe(datetime_is_numeric=True,
                                                include=None).T)
    logger.report_table(
                title=f"Ready-numerical-description-all",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df_num,
                    )
    descr_df_cat = pd.DataFrame(final_df.describe(datetime_is_numeric=True,
                                                include=object).T)
    logger.report_table(
                title=f"Ready-categorical-description-all",
                series="pandas DataFrame",
                iteration=0,
                table_plot=descr_df_cat,
                    )
    # clean_dataset.upload(show_progress=True)
    preprocessed_dataset.finalize()
    min_scale = final_df.rating.min()
    max_scale = final_df.rating.max()
    rating_scale = {'min': min_scale, 'max': max_scale}
    print(rating_scale)
    # A reader is still needed but only the rating_scale param is required.
    reader = sp.Reader(rating_scale=(min_scale, max_scale))
    prepared_df = sp.Dataset.load_from_df(final_df, reader)

    # train_data = prepared_df.build_full_trainset()
    # test_data = train_data.build_anti_testset()
    # train_data.to_csv(output_paths[0], index=False)
    # test_data.to_csv(output_paths[1], index=False)
    # task.close()
    return prepared_df


if __name__ == '__main__':
    prepare_dataset(source_dataset_name='preprocessed-data',
                    rating_column = 'rating_sum_S_and_P')
