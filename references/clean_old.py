"""
add because pylint
This module load and clean raw data
"""
# -*- coding: utf-8 -*-
import os
# from pathlib2 import Path
import pandas as pd
import numpy as np
import streamlit as st
from typing import List
# import click

# import pickle



DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')
RAW_DATA_PATH = os.path.join(*[DATA_PATH, 'raw', 'market_sales.csv'])
# CLEANED_DATA_PATH = os.path.join(DATA_PATH, 'interim', 'data_cleaned.csv')
def clean(filename=RAW_DATA_PATH):
    
    df = pd.read_csv(filename,
                    skiprows = 1,header = None, parse_dates=[0],
                    names=['period', 'user_id',
                            'store_id', 'item_id',
                            # 'sum_sale': np.float64,
                            # 'quantity':np.float64,
                            'license',
                            'type_by_nomenclature', 'rating'],
                    dtype={'user_id': str,
                            'store_id': str,
                            'item_id': str,
                            # 'sum_sale': np.float64,
                            # 'quantity':np.float64,
                            'license': np.int8,
                            'type_by_nomenclature': str,
                            'rating': np.int32})

    st.write('Dataframe cleaning starts')

    # deleting spaces in string columns
    df['user_id'] = df['user_id'].str.strip()
    df['store_id'] = df['store_id'].str.strip()
    df['item_id'] = df['item_id'].str.strip()

    df['type_by_nomenclature'] = df.type_by_nomenclature.fillna('D')

    # deleting wrong user_id
    df_clean = df[df.user_id.str.len() != 74]

    # deleting users with a lot of purchases and who have a purchases in more than 2 stores
    users_activity = df_clean.groupby('user_id').agg({'store_id': 'unique',
                                                    'item_id': 'count'}).reset_index()
    users_activity.rename(columns = {'item_id': 'num_purchases',
                                    'period': 'first_visit'}, inplace=True)
    anomaly_users = users_activity[(users_activity.num_purchases > 1000)]
    anomaly_users = anomaly_users.append(
        users_activity[(users_activity.store_id.str.len() > 2)]
    )
    anomaly_users = anomaly_users.append(users_activity[users_activity.num_purchases == 1])
    df_clean = df_clean[~df_clean.user_id.isin(anomaly_users.user_id.to_list())]

    del anomaly_users
    del users_activity

    # deleting refunds with no purchases
    df_clean['idx'] = df_clean.index
    # refunds = df_clean[df_clean.cost < 0]
    # purchases_users_with_refunds = df_clean[
    #     (df_clean.user_id.isin(refunds.user_id.unique()))
    #     & (df_clean.cost > 0)]
    # refunds['abs_cost'] = np.abs(refunds.cost)
    # purchases_with_refunds = refunds.merge(purchases_users_with_refunds,
    #                                        how='inner',
    #                                        left_on=['user_id',
    #                                                 'store_id',
    #                                                 'item_id',
    #                                                 'abs_cost'],
    #                                        right_on=['user_id',
    #                                                  'store_id',
    #                                                  'item_id',
    #                                                  'cost'])
    # correct_refunds = refunds[refunds.idx.isin(purchases_with_refunds.idx_x.to_list())]
    # cleaned_data = df_clean[df_clean.idx.isin(correct_refunds.idx)]
    # cleaned_data = cleaned_data.append(df_clean[df_clean.cost > 0])
    # cleaned_data = cleaned_data[cleaned_data.columns[:-1]]
    cleaned_data = df_clean
    columns_list = cleaned_data.columns.to_list()


    print('Columns list:', columns_list)
    cleaned_data['year'] = cleaned_data.period.dt.year
    cleaned_data['p_month'] = cleaned_data.period.dt.month
    cleaned_data['p_day'] = cleaned_data.period.dt.day
    cleaned_data['r_month'] = np.where((cleaned_data.p_month < 13)
                                    &(cleaned_data.p_day < 13),
                                    cleaned_data.p_day,
                                    cleaned_data.p_month)

    cleaned_data['r_day'] = np.where((cleaned_data.p_month < 13)
                                    & (cleaned_data.p_day < 13),
                                    cleaned_data.p_month,
                                    cleaned_data.p_day)
    cleaned_data['correct_period'] = pd.to_datetime(cleaned_data['year'].astype('str')
                                                    + '-' + cleaned_data['r_month'].astype('str')
                                                    + '-' + cleaned_data['r_day'].astype('str'),
                                                    format = "%Y-%m-%d")
    columns_list.append('correct_period')
    cleaned_data = cleaned_data[columns_list]

    
    # del refunds
    # del purchases_users_with_refunds
    # del correct_refunds

    st.write(f'Number of purchases in the cleaned dataframe: {cleaned_data.shape[0]}')
    return cleaned_data