# -*- coding: utf-8 -*-
import os
import pandas as pd
import numpy as np
import streamlit as st
from typing import List
# import click
from clearml import Task, StorageManager


DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')

CLEANED_DATA_PATH = os.path.join(DATA_PATH, 'interim', 'data_cleaned.csv')
CLEANED_USER_DATA_PATH = os.path.join(DATA_PATH, 'interim','cleaned_user_data.csv')
STORE_POSITION = 1  # store position in the list ranked by number of sales
MIN_NUM_PURCHASES = 10 
# @click.command()
# @click.argument('input_filepath', type=click.Path(exists=True))
# @click.argument('output_filepath', type=click.Path())
# @click.argument('store_position', type=click.INT)
# @click.argument('min_num_purchases', type=click.INT)
def select_store(input_path=CLEANED_DATA_PATH, 
                 output_path=CLEANED_USER_DATA_PATH, 
                 store_position=STORE_POSITION, 
                 min_num_purchases=MIN_NUM_PURCHASES):
    
    task = Task.init(project_name="mlops-rec-sys", task_name="Step 3 select_store")
    
    task.execute_remotely(queue_name="default")
    args = {
        'dataset_task_id': 'a656520fefe24ec5ad6f7fb36473915c',
        'dataset_url': ' http://localhost:8081/mlops-rec-sys/Step%202%20clean_data.e0112bb7c755460d88438292b58870a0/artifacts/cleaned-dataset/data_cleaned.csv ' 
        
    }
    
    task.connect(args)
    print('Arguments: {}'.format(args))


    # get dataset from task's artifact
    if args['dataset_task_id']:
        dataset_upload_task = Task.get_task(task_id=args['dataset_task_id'])
        print('Input task id={} artifacts {}'.format(args['dataset_task_id'], list(dataset_upload_task.artifacts.keys())))
        # download the artifact
        input_path = dataset_upload_task.artifacts['cleaned-dataset'].get_local_copy()
    # get the dataset from a direct url
    elif args['dataset_url']:
        input_path = StorageManager.get_local_copy(remote_url=args['dataset_url'])
    else:
        raise ValueError("Missing dataset link")
    df = pd.read_csv(input_path)

    # getting an id store placed on :store_position place in store list sorted by purchases
    # получение идентификатора магазина, размещенного на месте :store_position в списке магазинов,
    # отсортированном по покупкам
    store = df['store_id'].value_counts().index[store_position]

    # # getting purchasing data by selected store
    # # получение данных о покупках по выбранному магазину
    one_store_data = df[df['store_id'] == store]

    st.write(f'Data collected for the store ranked {store_position} in terms of sales')

    users_by_purchases = one_store_data['user_id'].value_counts().reset_index()
    users_by_purchases = users_by_purchases.rename(columns={'index': 'user_id', 'user_id': 'purchases'})
    users_id_list = users_by_purchases[users_by_purchases['purchases'] > min_num_purchases]['user_id']
    users_filter = one_store_data['user_id'].isin(users_id_list)
    cleaned_user_data = one_store_data[users_filter]

    cleaned_user_data.to_csv(output_path, index=False)
    task.upload_artifact('cleaned-user-data', artifact_object=output_path)

if __name__ == "__main__":
    select_store()