# from https://github.com/allegroai/clearml/issues/411
import pandas as pd
from clearml import Task
import clearml
import time


task_name = "TASK_NAME"
task = Task.init(project_name="PROJECT_NAME", task_name=task_name)
task.execute_remotely(queue_name="default")


def get_batch_generator(number_of_batch=3, number_of_rows_per_batch=100):
    """
    dummy data generator
    """
    for _ in range(0, number_of_batch):
        yield pd.DataFrame([j for j in range(0, number_of_rows_per_batch)], columns=["col"])


# main task execution
if task.name == task_name:
    i = 0
    task_ids = []
    for batch in get_batch_generator():
        i += 1
        # upload batch artifact
        func_name = "batch_{}_{}".format(round(time.time() * 10000), i)
        task.upload_artifact(name=func_name, artifact_object=batch)

        # start and enqueue clearml subtask that will process it
        subtask = task.create_function_task(
            func=lambda x: x,
            func_name=func_name,
            task_name="{}_{}".format(task.name, func_name),
            artifact_name=func_name,
            task_id=task.id
        )
        task.enqueue(subtask, queue_name="default")
        task_ids.append(subtask.id)

    # wait for subtasks to finish
    for task_id in task_ids:
        clearml.Task.get_task(task_id=task_id).wait_for_status(check_interval_sec=30.0)

#subtask execution
else:
    parameters = task.get_parameters()

    parent_task_id = parameters["Function/task_id"]
    parent_task = Task.get_task(task_id=parent_task_id)
    print("parent task {} has the following artifacts: {}".format(parent_task_id, parent_task.artifacts))

    # retrieve the artifact (this generate the exception)
    batch = parent_task.artifacts[parameters["Function/artifact_name"]].get()

    # process the batch ...
