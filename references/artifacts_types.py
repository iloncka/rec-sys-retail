import os
from time import sleep

import pandas as pd
import numpy as np
from PIL import Image
from clearml import Task



DIR_PATH = os.path.dirname(__file__)
ROOT_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')
RAW_DATA_PATH = os.path.join(DATA_PATH, 'raw')
CLEANED_DATA_PATH = os.path.join(DATA_PATH, 'interim')
PREDICT_DATA_PATH = os.path.join(DATA_PATH,'processed')
TRAINED_MODELS_PATH = os.path.join(ROOT_PATH,'models')
# RAW_DATA_PATH, TRAINED_MODELS_PATH, PREDICT_DATA_PATH
# Connecting ClearML with the current process,
# from here on everything is logged automatically
task = Task.init(project_name='Learning examples', task_name='Artifacts example')

df = pd.DataFrame(
    {
        'num_legs': [2, 4, 8, 0],
        'num_wings': [2, 0, 0, 0],
        'num_specimen_seen': [10, 2, 1, 8]
    },
    index=['falcon', 'dog', 'spider', 'fish']
)

# Register Pandas object as artifact to watch
# (it will be monitored in the background and automatically synced and uploaded)
task.register_artifact('train', df, metadata={'counting': 'legs', 'max legs': 69})
# change the artifact object
df.sample(frac=0.5, replace=True, random_state=1)
# or access it from anywhere using the Task's get_registered_artifacts()
Task.current_task().get_registered_artifacts()['train'].sample(frac=0.5, replace=True, random_state=1)

# add and upload pandas.DataFrame (onetime snapshot of the object)
task.upload_artifact('Pandas', artifact_object=df)
# add and upload local file artifact
task.upload_artifact('local file', artifact_object=os.path.join(RAW_DATA_PATH,'market_sales.csv'))
# add and upload dictionary stored as JSON)
task.upload_artifact('dictionary', df.to_dict())
# add and upload Numpy Object (stored as .npz file)
task.upload_artifact('Numpy Eye', np.eye(100, 100))
# # add and upload Image (stored as .png file)
# im = Image.open(os.path.join('data_samples', 'dancing.jpg'))
# task.upload_artifact('pillow_image', im)
# add and upload a folder, artifact_object should be the folder path
task.upload_artifact('local folder', artifact_object=PREDICT_DATA_PATH)
# add and upload a wildcard
task.upload_artifact('wildcard jpegs', artifact_object=os.path.join(TRAINED_MODELS_PATH, '*.pkl'))
# do something here
sleep(1.)
print(df)

# we are done
print('Done')