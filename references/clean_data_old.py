"""
add because pylint
This module load and clean row data
"""
# -*- coding: utf-8 -*-
import os
# from pathlib2 import Path
import pandas as pd
import numpy as np
import streamlit as st
from typing import List
# import click
import pickle
from clearml import Task, StorageManager


DIR_PATH = os.path.dirname(__file__)
SRC_PATH = os.path.normpath(os.path.join(DIR_PATH, os.pardir))
ROOT_PATH = os.path.normpath(os.path.join(SRC_PATH, os.pardir))
DATA_PATH = os.path.join(ROOT_PATH, 'data')
RAW_DATA_PATH = os.path.join(DATA_PATH, 'raw', 'market_sales.csv')
CLEANED_DATA_PATH = os.path.join(DATA_PATH, 'interim', 'data_cleaned.csv')

# URL = 'https://link.storjshare.io/s/jwc5svdn2t7v7gbby6c3zvcivdcq/database/market_sales.csv'
# URL = 'https://www.dropbox.com/s/nhswkhngncsi8n6/Data_rec_sys_row.csv?dl=1'
# @click.command()
# @click.argument('input_filepath', type=click.Path(exists=True))
# @click.argument('output_filepath', type=click.Path())
def clean_data(input_path=RAW_DATA_PATH, output_path=CLEANED_DATA_PATH):
    # Connecting ClearML with the current process,
    # from here on everything is logged automatically
    task = Task.init(project_name="mlops-rec-sys", task_name="Step 2 clean_data v2")

    # task.execute_remotely(queue_name="default")
    # program arguments
    # Use either dataset_task_id to point to a tasks artifact or
    # use a direct url with dataset_url
    args = {
        'dataset_task_id': 'f376c06834f548319544df0f310f6b0c',
        'dataset_url': '' 
        
    }

    # store arguments, later we will be able to change them from outside the code
    task.connect(args)
    print('Arguments: {}'.format(args))


    # get dataset from task's artifact
    if args['dataset_task_id']:
        dataset_upload_task = Task.get_task(task_id=args['dataset_task_id'])
        print('Input task id={} artifacts {}'.format(args['dataset_task_id'], list(dataset_upload_task.artifacts.keys())))
        # download the artifact
        input_path = dataset_upload_task.artifacts['raw-dataset'].get_local_copy()
    # get the dataset from a direct url
    elif args['dataset_url']:
        input_path = StorageManager.get_local_copy(remote_url=args['dataset_url'])
    else:
        raise ValueError("Missing dataset link")



    # "process" data
    # input_path = os.path.join(RAW_DATA_PATH, 'market_sales.csv')
    df = pd.read_csv(input_path,
                        skiprows = 1,header = None, parse_dates=[0],
                        names=['period', 'user_id',
                                'store_id', 'item_id',
                                'license',
                                'type_by_nomenclature', 'rating'],
                        dtype={'user_id': str,
                                'store_id': str,
                                'item_id': str,

                                'license': np.int8,
                                'type_by_nomenclature': str,
                                'rating': np.int32})

    st.write('Dataframe cleaning starts')

    # deleting spaces in string columns
    df['user_id'] = df['user_id'].str.strip()
    df['store_id'] = df['store_id'].str.strip()
    df['item_id'] = df['item_id'].str.strip()

    df['type_by_nomenclature'] = df.type_by_nomenclature.fillna('D')

    # deleting wrong user_id
    df_clean = df[df.user_id.str.len() != 74]

    # deleting users with a lot of purchases and who have a purchases in more than 2 stores
    users_activity = df_clean.groupby('user_id').agg({'store_id': 'unique',
                                                      'item_id': 'count'}).reset_index()
    users_activity.rename(columns = {'item_id': 'num_purchases',
                                     'period': 'first_visit'}, inplace=True)
    anomaly_users = users_activity[(users_activity.num_purchases > 1000)]
    anomaly_users = anomaly_users.append(
        users_activity[(users_activity.store_id.str.len() > 2)]
    )
    anomaly_users = anomaly_users.append(users_activity[users_activity.num_purchases == 1])
    df_clean = df_clean[~df_clean.user_id.isin(anomaly_users.user_id.to_list())]

    del anomaly_users
    del users_activity

    # deleting refunds with no purchases
    df_clean['idx'] = df_clean.index
    # refunds = df_clean[df_clean.cost < 0]
    # purchases_users_with_refunds = df_clean[
    #     (df_clean.user_id.isin(refunds.user_id.unique()))
    #     & (df_clean.cost > 0)]
    # refunds['abs_cost'] = np.abs(refunds.cost)
    # purchases_with_refunds = refunds.merge(purchases_users_with_refunds,
    #                                        how='inner',
    #                                        left_on=['user_id',
    #                                                 'store_id',
    #                                                 'item_id',
    #                                                 'abs_cost'],
    #                                        right_on=['user_id',
    #                                                  'store_id',
    #                                                  'item_id',
    #                                                  'cost'])
    # correct_refunds = refunds[refunds.idx.isin(purchases_with_refunds.idx_x.to_list())]
    # cleaned_data = df_clean[df_clean.idx.isin(correct_refunds.idx)]
    # cleaned_data = cleaned_data.append(df_clean[df_clean.cost > 0])
    # cleaned_data = cleaned_data[cleaned_data.columns[:-1]]
    cleaned_data = df_clean
    columns_list = cleaned_data.columns.to_list()
    
    
    print('Columns list:', columns_list)
    cleaned_data['year'] = cleaned_data.period.dt.year
    cleaned_data['p_month'] = cleaned_data.period.dt.month
    cleaned_data['p_day'] = cleaned_data.period.dt.day
    cleaned_data['r_month'] = np.where((cleaned_data.p_month < 13)
                                       &(cleaned_data.p_day < 13),
                                       cleaned_data.p_day,
                                       cleaned_data.p_month)

    cleaned_data['r_day'] = np.where((cleaned_data.p_month < 13)
                                     & (cleaned_data.p_day < 13),
                                     cleaned_data.p_month,
                                     cleaned_data.p_day)
    cleaned_data['correct_period'] = pd.to_datetime(cleaned_data['year'].astype('str')
                                                    + '-' + cleaned_data['r_month'].astype('str')
                                                    + '-' + cleaned_data['r_day'].astype('str'),
                                                    format = "%Y-%m-%d")
    columns_list.append('correct_period')
    cleaned_data = cleaned_data[columns_list]

    cleaned_data.to_csv(output_path, index=False)
    
    task.upload_artifact('cleaned-dataset', artifact_object=output_path)

    print('Uploading artifacts in the background ...')

    # we are done
    print('Done')
    # del refunds
    # del purchases_users_with_refunds
    # del correct_refunds

    st.write(f'Number of purchases in the cleaned dataframe: {cleaned_data.shape[0]}')


if __name__ == "__main__":
    
    clean_data()
